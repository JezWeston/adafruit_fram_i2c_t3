The sketch tests and times reading and writing from a Teensy 3.1 to the Adafruit FRAM breakout board.

Output is via Serial.

Serial output should be something like:

=========================
loop()
-------------------------
Going to test single byte writes and reads
Time taken for 1000 writes with one byte was: 60715 us
Write time per byte, in microseconds:	60
-------------------------
Time taken for 1000 reads with one byte was: 88225 us
Read time per byte, in microseconds:	88
-------------------------
Going to test readBlock
Using 1000 blocks of 4
Time taken for 1000 reads with blocks was: 122818 us
Read time per byte, in microseconds:	30
-------------------------
Going to test readBlock
Using 1000 blocks of 8
Time taken for 1000 reads with blocks was: 168535 us
Read time per byte, in microseconds:	21
-------------------------
Going to test readBlock
Using 1000 blocks of 16
Time taken for 1000 reads with blocks was: 259891 us
Read time per byte, in microseconds:	16
-------------------------
Going to test readBlock
Using 1000 blocks of 32
Time taken for 1000 reads with blocks was: 442840 us
Read time per byte, in microseconds:	13
-------------------------
Going to test readBlock
Using 1000 blocks of 64
Time taken for 1000 reads with blocks was: 808583 us
Read time per byte, in microseconds:	12
-------------------------
Going to test readBlock
Using 1000 blocks of 128
Time taken for 1000 reads with blocks was: 1540300 us
Read time per byte, in microseconds:	12
-------------------------
Going to test writeBlock
Using 1000 blocks of 4
Time taken for 1000 writes with blocks was: 379995 us
Write time per byte, in microseconds:	94
-------------------------
Going to test writeBlock
Using 1000 blocks of 8
Time taken for 1000 writes with blocks was: 1112241 us
Write time per byte, in microseconds:	139
-------------------------
Going to test writeBlock
Using 1000 blocks of 16
Time taken for 1000 writes with blocks was: 3632772 us
Write time per byte, in microseconds:	227
-------------------------
Going to test writeBlock
Using 1000 blocks of 32
Time taken for 1000 writes with blocks was: 12884163 us
Write time per byte, in microseconds:	402
=========================
