// Test sketch for the Adafruit FRAM breakout board using the MB85RC256V memory.
// Modified to replace Wire.h with i2c_t3.h by Jez Weston
// On a Teensy 3.1 at 48 MHz and I2C at 2 MHz (I2C_RATE_2000), single byte reads and writes take 88 microseconds and 60 microseconds respectively.
// Reading in blocks is faster and depends upon the size of blocks.
// Reading 64 bytes or more takes 12 microseconds per byte.
// Writing in blocks is slower than writing single bytes. I don't know why.

#include "i2c_t3.h" // Can be down loaded from http://forum.pjrc.com/threads/21680-New-I2C-library-for-Teensy3

#include "Adafruit_FRAM_i2c_t3.h"

/* Example code for the Adafruit I2C FRAM breakout */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 5.0V DC
   Connect GROUND to common ground */

Adafruit_FRAM_I2C fram = Adafruit_FRAM_I2C();
uint16_t framAddr = 0;

int led = 13;
uint32_t startTime;	// microseconds
uint32_t timeTaken;
 
const bool DISPLAY_WRITTEN_DATA = false;

void reportTimeTaken(const char* str, uint32_t timeTaken) {
  Serial.print("Time taken for ");
  Serial.print(str);
  Serial.print(" was: ");
  Serial.print(timeTaken);
  Serial.println( " us");
}


// Test write speed for blocks
void testBlockWrite(uint8_t blockSize) {
  uint8_t writeBlock[blockSize];
  // Set up test data
  for (uint8_t b = 0; b < blockSize; b++) {
   writeBlock[b] = b;
  }
  
  // Set up test reporting
  Serial.println("-------------------------");
  Serial.println("Going to test writeBlock");
  Serial.print("Using 1000 blocks of ");
  Serial.println(blockSize);

  // Run test and report
  startTime = micros();
  
  for (uint16_t a = 0; a < 1000; a++) {
    for (uint16_t b = 0; b < blockSize; b++) {
      fram.writeBlock(b*blockSize, writeBlock, blockSize);
    }
  }
  timeTaken = micros() - startTime;	// Note: Doesn't check for roll-over here. Happens every day or so.
  reportTimeTaken("1000 writes with blocks", timeTaken);
  Serial.print("Write time per byte, in microseconds:\t");
  Serial.println(timeTaken/(blockSize*1000));
  
  
  if (DISPLAY_WRITTEN_DATA) {
    // Show written data to check write was successful
    uint8_t valueBlock[blockSize];
    Serial.println("Block write wrote:");
    for (uint16_t a = 0; a < blockSize; a++) {
      fram.readBlock(a*blockSize, valueBlock, blockSize);
      Serial.print("Block: ");
      Serial.print(a, HEX);
      Serial.print(":\t");
      for ( uint16_t b=0; b<blockSize; b++) {
        Serial.print("0x"); 
        if (valueBlock[b] < 0x1) 
          Serial.print('0');
        Serial.print(valueBlock[b], HEX); Serial.print(" ");
      }
      Serial.println();
    }
  }
}


void testBlockRead(uint8_t blockSize) {
  uint8_t valueBlock[blockSize];

  // Set up test reporting
  Serial.println("-------------------------");
  Serial.println("Going to test readBlock");
  Serial.print("Using 1000 blocks of ");
  Serial.println(blockSize);
  
  // Run test and report
  startTime = micros();
  for (uint16_t a = 0; a < 1000; a++) {
    fram.readBlock(a*blockSize, valueBlock, blockSize);
  }
  timeTaken = micros() - startTime;	// Note: Doesn't check for roll-over here. Happens every day or so.
  reportTimeTaken("1000 reads with blocks", timeTaken);
  Serial.print("Read time per byte, in microseconds:\t");
  Serial.println(timeTaken/(blockSize*1000));
}


void setup(void) {
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);

  Serial.begin(9600);
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);
  if (fram.begin(0x50)) {  // you can stick the new i2c addr in here, e.g. begin(0x51);
    Serial.println("Found I2C FRAM");
  } else {
    Serial.println("No I2C FRAM found ... check your connections\r\n");
    while (1);
  }

  digitalWrite(led, LOW);   // turn the LED off
  // Set up test data
  uint8_t test = fram.read8(0x0);
  for (uint16_t a = 0; a < 32768; a++) {
    fram.write8(a, test++);
  }
}


void loop(void) {
  // Heartbeat
  digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);               // wait for a second
  digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
  delay(100);               // wait for a second
  Serial.println("=========================");
  Serial.println("loop()");

  Serial.println("-------------------------");
  Serial.println("Going to test single byte writes and reads");
  uint8_t test;
  // Test write speed
  startTime = micros();
  for (uint16_t a = 0; a < 1000; a++) {
    fram.write8(a, test++);
  }
  timeTaken = micros() - startTime;	// Note: Doesn't check for roll-over here. Happens every day or so.
  reportTimeTaken("1000 writes with one byte", timeTaken);
  Serial.print("Write time per byte, in microseconds:\t");
  Serial.println(timeTaken/1000);

  Serial.println("-------------------------");
  // Test read speed for single bytes
  uint8_t value;
  startTime = micros();
  for (uint16_t a = 0; a < 1000; a++) {
    value = fram.read8(a);
  }
  timeTaken = micros() - startTime;	// Note: Doesn't check for roll-over here. Happens every day or so.
  reportTimeTaken("1000 reads with one byte", timeTaken);
  Serial.print("Read time per byte, in microseconds:\t");
  Serial.println(timeTaken/1000);

  testBlockRead(4);
  testBlockRead(8);
  testBlockRead(16);
  testBlockRead(32);
  testBlockRead(64);
  testBlockRead(128);

  testBlockWrite(4);
  testBlockWrite(8);
  testBlockWrite(16);
  testBlockWrite(32);
}
