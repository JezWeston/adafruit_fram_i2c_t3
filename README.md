#Adafruit I2C FRAM Driver #

This driver is for the Adafruit I2C FRAM breakout.
    ------> http://www.adafruit.com/products/1895

## About this Driver ##

These modules use I2C to communicate, 2 pins are required to  
interface

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Kevin (KTOWN) Townsend for Adafruit Industries.
BSD license, check license.txt for more information
All text above must be included in any redistribution

To download. click the ZIP button in the top bar, and check this tutorial
for instructions on how to install: 
http://learn.adafruit.com/adafruit-all-about-arduino-libraries-install-use

## Changes from the original Adafruit library ##

Modified by Jez Weston (http://www.happyinmotion.com) to run on a Teensy 3.1 
and to run I2c using the i2c_t3 library rather than Wire.

Wire is horrible.

This change makes the FRAM access much faster. Reads (in blocks) can be 30x 
faster. Writes can be 5x faster.

More discussion of these changes at:
http://forum.pjrc.com/threads/25885-Any-experiences-with-FeRAM-s
